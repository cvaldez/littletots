// When the window has finished loading create our google map below
  google.maps.event.addDomListener(window, 'load', init);

  function init() {
      // Basic options for a simple Google Map
      // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
	  /*
	  var baseAddress = "Calle Tancítaro 3408 Col. San Jerónimo Esq. Tres Oriente León, Guanajuato, MX";
	  var basePhone = "477 270 11 51";
	  var baseEmail = "info@ecotapa.com.mx";
	  var blankSpace = "&nbsp;";*/
	  
      var mapOptions = {
          // How zoomed in you want the map to start at (always required)
          zoom: 20,
          scrollwheel: true,

          // The latitude and longitude to center the map (always required)
          center: new google.maps.LatLng(21.1504309, -101.6698454)//, //Ecotapa Address

          // How you would like to style the map. 
          // This is where you would paste any style found on Snazzy Maps.
          /*styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 21 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 19 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }]*/
      };

      // Get the HTML DOM element that will contain your map 
      // We are using a div with id="map" seen below in the <body>
      var mapElement = document.getElementById('map');
	 
      // Create the Google Map using our element and options defined above
      var map = new google.maps.Map(mapElement, mapOptions);


      var contentString = '<div id="map-content">' +
          '<div id="bodyContent">' +
          '<p> <span class="map-info-icon"><i class="icofont icofont-social-google-map"></i></span> <span class="map-info-content">Calle Tancítaro 3408 Col. San Jerónimo Esq. Tres Oriente </br> ' +
          ' León, Guanajuato, MX </span></p> ' +
          '<p> <span class="map-info-icon"><i class="icofont icofont-phone"></i></span> <span> <a href="https://api.whatsapp.com/send?phone=00524772701151">477 270 11 51</a></span> </p> ' +
          '</div>' +
          '</div>';

      var infowindow = new google.maps.InfoWindow({
          content: contentString
      });

      // Let's also add a marker while we're at it
      var marker = new google.maps.Marker({
          position: map.getCenter(),
          icon: 'img/icons/map-marker-icon.png',
          map: map,
          title: baseAddress
      });
          infowindow.open(map, marker);
  }
