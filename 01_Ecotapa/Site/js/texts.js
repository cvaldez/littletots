(function($) {
    'use strict';

  
	//HTML MARKUP AND GENERICS
	var blankSpace = "&nbsp;";
	  
	  
	  
	//INDEX PAGE
	
	
	
	
	
	
	//ABOUT THE ENTERPRISE
	var about_title1 = "Acerca de";
	var about_title2 = "nosotros";
	var about_text1 = "Somos una empresa con m&aacute;s de 10 a&ntilde;os de experiencia en el ramo, sabemos lo que hacemos. Entre las ventajas que tienes con nosotros est&aacute;n las siguientes: ";
	var about_card1_title = "Experiencia";
	var about_card1_text = "Con m&aacute;s de 10 a&ntilde;os de experiencia en el ramo de plantas purificadoras de agua, conocemos el negocio desde la primera instalaci&oacute;n hasta tus ventas y crecimiento.";
	var about_card2_title = "Respaldo";
	var about_card2_text = "Tenemos a t&eacute;cnicos expertos capaces de resolver cualquier problema adecuado a sus necesidades.";
	var about_card3_title = "Prestigio";
	var about_card3_text = "Somos conocidos y respaldados por el medio de proveedores para purificadoras m&aacute;s calificados del baj&iacute;o.";
	var about_card4_title = "Calidad de Servicio";
	var about_card4_text = "Desde la primer asesor&iacute;a, hasta el primer garraf&oacute;n e incluyendo los posibles problemas que pudieras tener, todo est&aacute; cubierto con nosotros.";
	
	//OUR PRODUCTS
	var products_text = "Somos proveedores en la regi&oacute;n de muchas de las plantas pufificadoras.";
	
	
	//SERVICES
	var services_text1="Si usted quiere poner su propio negocio tambi&eacuten le podemos ayudar, ya que, con toda nuestra expreiencia podemos hacer de su emprendimiento una inversi&oacute;n segura.";
	var services_card_title1="Instalaci&oacute;n de equipos";
	var services_card_title2="Venta";
	var services_card_title3="Mantenimiento";
	var services_card_title4="Asesor&iacute;a";
	var services_card_text1="Ponemos a punto el lugar donde quieras instalar tu planta purificadora con est&aacute;ndares de clase mundial.";
	var services_card_text2="Venta de toda clase de insumos para negocio, contamos con local e inventario constante.";
	var services_card_text3="Por calidad todo equipo de purificaci&oacute;n de agua est&aacute; debe tener periodos de mantenimiento de la misma calidad que tu negocio lo requiere.";
	var services_card_text4="No solo desarrollamos negocios sino que tambi&eacute; podemos capacitarte para la f&aacute;cil operaci&oacute;n y resoluci&oacute;n de problemas.";
	
	
	
	
	//FOOTER DATA
	  var footer_baseAddress = "Calle Tanc&iacute;taro 3408 Col. San Jer&oacute;nimo Esq. Tres Oriente Le&oacute;n, Guanajuato, MX";
	  var footer_basePhone = "477 270 11 51";
	  var footer_baseEmail = "info@ecotapa.com.mx";
	  var footer_ecotapaText = "Comercializadora de insumos para purificadoras de agua";
	  var footer_baseAtnHours = blankSpace+blankSpace+"Lun-Vie 9:00am - 6:00pm<br/>"+blankSpace+blankSpace+blankSpace+blankSpace+blankSpace+"S&aacute;b 9:00am - 2:00pm";
	
	
	/*USING THE DOM */
	//ZONE: INDEX
	
	
	//ZONE: ABOUT US
	 $("#about_title1").html(about_title1);
	 $("#about_title2").html(about_title2);
	 $("#about_text1").html(about_text1);
	 $("#about_card1_title").html(about_card1_title);
	 $("#about_card2_title").html(about_card2_title);
	 $("#about_card3_title").html(about_card3_title);
	 $("#about_card4_title").html(about_card4_title);
	 $("#about_card1_text").html(about_card1_text);
	 $("#about_card2_text").html(about_card2_text);
	 $("#about_card3_text").html(about_card3_text);
	 $("#about_card4_text").html(about_card4_text);
	
	
	
	
	
	//ZONE: OUR SERVICES
	$("#services_text1").html(services_text1);
	$("#services_card_title1").html(services_card_title1);
	$("#services_card_title2").html(services_card_title2);
	$("#services_card_title3").html(services_card_title3);
	$("#services_card_title4").html(services_card_title4);
	$("#services_card_text1").html(services_card_text1);
	$("#services_card_text2").html(services_card_text2);
	$("#services_card_text3").html(services_card_text3);
	$("#services_card_text4").html(services_card_text4);
	
	
	//ZONE:FOOTER
	 $("#baseAddress").html(blankSpace +blankSpace+ footer_baseAddress);
	 $("#basePhone").html(blankSpace +blankSpace+ footer_basePhone);
	 $("#baseMail").html(blankSpace +blankSpace+ footer_baseEmail);
	 $("#baseEntepriseText").html(footer_ecotapaText);
	 $("#baseHour").html(footer_baseAtnHours);
	 $("#baseMail").css("m-5");
	
})(jQuery);
